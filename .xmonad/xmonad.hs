import Control.Monad (when)
import System.Exit (exitSuccess)
import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Layout.NoBorders
import XMonad.Hooks.FadeInactive (fadeInactiveLogHook)
import XMonad.Util.Dmenu

import XMonad.Layout.MouseResizableTile
import qualified Data.Map as M (fromList, union)

main :: IO ()
main = do
  cfg <- statusBar "xmobar" myPP toggleStrutsKey def
        { modMask = mod4Mask
        , terminal = "terminator"
        , layoutHook = myLayout
        , normalBorderColor = "#111111"
        , focusedBorderColor = "#4488AA"
        , borderWidth = 1
        , logHook = myLogHook
        , startupHook = spawn "$HOME/bin/xm-startup"
        , keys = M.union <$> keys' <*> keys def
        }
  xmonad cfg

myPP :: PP
myPP = xmobarPP

keys' conf@(XConfig {XMonad.modMask = modMask}) = M.fromList [
    ((modMask .|. shiftMask, xK_q), quitWithWarning)
  ]

toggleStrutsKey :: XConfig l -> (KeyMask, KeySym)
toggleStrutsKey XConfig { XMonad.modMask = modMask' } = (modMask', xK_b)

myLogHook = do
  --return ()
   fadeInactiveLogHook 0.9

myLayout = tiled ||| Mirror tiled ||| noBorders Full
  where
    tiled = Tall
      { tallNMaster = 1
      , tallRatio = 1/2
      , tallRatioIncrement = 3/100
      }

resizableLayout = tiled ||| mirrorTiled ||| noBorders Full
--myLayout = tiled ||| mirrorTiled ||| Full
  where
    tiled = mouseResizableTile
        { isMirrored = False
        , draggerType = BordersDragger
        , nmaster = 1
        , masterFrac = 1/2
        , fracIncrement = 3/100
        }
    mirrorTiled = mouseResizableTile
        { isMirrored = True
        , draggerType = BordersDragger
        , nmaster = 1
        , masterFrac = 1/2
        , fracIncrement = 3/100
        }

quitWithWarning :: X ()
quitWithWarning = do
  let m = "confirm quit"
  s <- dmenu [m]
  when (m == s) (io exitSuccess)
