export DOTNET_CLI_TELEMETRY_OPTOUT=1
export FUNCTIONS_CORE_TOOLS_TELEMETRY_OPTOUT=1

export EDITOR=vim

if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ]; then
    PROMPT='%(?.%F{green}👍.%F{red}👎) [%D{%F} %D{%L:%M:%S}] %F{cyan}% %n@%m %~'$'\n''%(?.%F{green}.%F{red})>%F{white} '
else
    PROMPT='%(?.%F{green}👍.%F{red}👎) [%D{%F} %D{%L:%M:%S}] %F{cyan}%~'$'\n''%(?.%F{green}.%F{red})>%F{white} '
fi

RPROMPT=\$vcs_info_msg_0_

HISTFILE=~/.histfile
HISTSIZE=100000
SAVEHIST=100000

alias ls='ls --color'
alias tmux='tmux -2'
alias xc='xclip -selection clipboard'
alias runghc='runghc -Wall'

function unreddit {
  if [ -z ${1+x} ];
    # If there is no argument, update the clipboard.
    then xc -o | sed 's/reddit\.com/teddit.net/g' | xc;
    else echo $1 | sed 's/reddit\.com/teddit.net/g';
  fi
}

# . ~/.nix-profile/etc/profile.d/nix.sh

export GPG_TTY="$(tty)"
export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
gpgconf --launch gpg-agent

export FZF_DEFAULT_OPTS='--height 40% --reverse'
export FZF_DEFAULT_COMMAND='ag -g ""'

# Lines configured by zsh-newuser-install
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/chris/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

export PATH="${HOME}/bin:${PATH}"

autoload -Uz vcs_info
precmd_vcs_info() { vcs_info }
precmd_functions+=( precmd_vcs_info )
setopt prompt_subst
zstyle ':vcs_info:git:*' formats '%F{13}%b %u%f'
zstyle ':vcs_info:*' enable git

export PATH="$HOME/.local/bin:$PATH"
export PATH="$HOME/.cargo/bin:$PATH"
export PATH="${HOME}/.npm-packages/bin:${PATH}"

alias mpv='mpv --ytdl-raw-options=no-check-certificate='
alias ytdl="youtube-dl --no-check-certificate"
alias ytdl-audio="youtube-dl --no-check-certificate -x"
alias ec='emacsclient -n'
alias ding='(mpv --really-quiet --end=2 ~/Media/Sounds/ding.mp3 &)'
alias alert='(mpv --really-quiet --end=2 ~/Media/Sounds/alert.wav &)'

export LEDGER_FILE=~/org/finance/2021.journal

if [ -e /home/chris/.nix-profile/etc/profile.d/nix.sh ]; then . /home/chris/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer

# I have no idea where this is set, but unsetting it fixes the error "warning: setlocale: LC_ALL: cannot change locale (en_US.utf8)"
# unset LC_ALL

alias ,dot='git --git-dir "$HOME/repos/dotfiles" --work-tree="$HOME"'
alias ,trim='sed '"'"'s/^[ \t]*//;s/[ \t]*$//'"'"
